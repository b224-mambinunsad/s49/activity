// alert("Hello")

// fetch() method in JS is used to sednd request in the server and load the received response in the webpage/s. The request and response is in JSON format.

/*

	Syntax: 
		fetch("url", {option})
			url - this is the address which the request is to be made and source where the response will come from (endpoint)
			options - array or properties that contains the HTTPS method, body of request and headers.

*/


// Get Post Data / Retrieve functionn
fetch("https://jsonplaceholder.typicode.com/posts")
.then((response) => response.json())
.then((data) => showPosts(data));


/*
	Mini-Activity:
		Retrieve a single post from JSON API and print it in the console.

*/

// fetch("https://jsonplaceholder.typicode.com/posts/99")
// .then((response) => response.json())
// .then((data) => console.log(data));



// ADD POST DATA
document.querySelector("#form-add-post").addEventListener("submit", (e) => {

	e.preventDefault();

	fetch("https://jsonplaceholder.typicode.com/posts", {
		method: "POST", 
		body: JSON.stringify({
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value,
			userId: 1
		}),
		headers: {
			"Content-Type": "application/json"
		}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data)
		alert("Post Successfully Added!")
	})

	// reset the value
	document.querySelector("#txt-title").value = null;
	document.querySelector("#txt-body").value = null;
	
});



// VIEW POST - used to dispaly each post from JSON place holder
const showPosts = (posts) => {
	//Create a varaible that will contain all the posts.
	let postEntries = "";


	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	});

	// To check what is stored in the postEntries variables.
	// console.log(postEntries)

	// To assign the value of postEntries to the element with "div-post-entries" id.
	document.querySelector("#div-post-entries").innerHTML = postEntries

};


// EDIT POST DATA / Edit Button

const editPost = (id) => {

	// Displayed post in the post section (source of data)
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	// Edit post form elements (reciever of data)
	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;

	document.querySelector("#btn-submit-update").removeAttribute("disabled")
};


// UPDATE POST FUNCTION (Update button in the edit post form)
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
	e.preventDefault();

	let id = document.querySelector("#txt-edit-id").value

	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
		method: "PUT",
		body: JSON.stringify({
			id: id,
			title: document.querySelector("#txt-edit-title").value,
			body: document.querySelector("#txt-edit-body").value,
			userId: 1
		}),
		headers: {
			"Content-Type": "application/json"
		}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data)
		alert("Post Successfully Updated")
	});

	 document.querySelector("#txt-edit-title").value = null;
	 document.querySelector("#txt-edit-body").value = null;

	 document.querySelector("#btn-submit-update").setAttribute("disabled", true)

});



// DELETE POST

const deletePost = (id) => {
	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
		method: "DELETE"
	})
	.then(response => console.log(response))

	const post = document.querySelector(`#post-${id}`)
	post.remove()
	alert("Successfully Remove a Post")
}

